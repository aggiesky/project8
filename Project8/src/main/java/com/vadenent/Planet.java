package com.vadenent;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.validation.constraints.NotEmpty;

@Entity
public class Planet {

	@javax.persistence.Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	@NotEmpty
	private String planetName;
	private int distanceFromSun;
	private int lengthOfYear;
	private String lengthOfYearUnit;
	private String imageName;
	private boolean hasMoons;
	private String numberOfMoons;
	private boolean isTerrestrial;
	private String atmosphereDescription;

	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPlanetName() {
		return planetName;
	}
	public void setPlanetName(String planetName) {
		this.planetName = planetName;
	}
	public int getDistanceFromSun() {
		return distanceFromSun;
	}
	public void setDistanceFromSun(int distanceFromSun) {
		this.distanceFromSun = distanceFromSun;
	}
	public int getLengthOfYear() {
		return lengthOfYear;
	}
	public void setLengthOfYear(int lengthOfYear) {
		this.lengthOfYear = lengthOfYear;
	}
	public String getLengthOfYearUnit() {
		return lengthOfYearUnit;
	}
	public void setLengthOfYearUnit(String lengthOfYearUnit) {
		this.lengthOfYearUnit = lengthOfYearUnit;
	}
	public String getImageName() {
		return imageName;
	}
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}
	public boolean getHasMoons() {
		return hasMoons;
	}
	public void setHasMoons(boolean hasMoons) {
		this.hasMoons = hasMoons;
	}

	public String getNumberOfMoons() {
		return numberOfMoons;
	}
	public void setNumberOfMoons(String numberOfMoons) {
		this.numberOfMoons = numberOfMoons;
	}

	public boolean getIsTerrestrial() {
		return isTerrestrial;
	}
	public void setIsTerrestrial(boolean isTerrestrial) {
		this.isTerrestrial = isTerrestrial;
	}

	public String getAtmosphereDescription() {
		return atmosphereDescription;
	}
	public void setAtmosphereDescription(String atmosphereDescription) {
		this.atmosphereDescription = atmosphereDescription;
	}

	public Planet getEmpty() {
		Planet newPlanet = new Planet();
		return newPlanet;
	}
	
	@Override
	public String toString() {
		return "Planet [Id=" + id + ", planetName=" + planetName + ", distanceFromSun=" + distanceFromSun
				+ ", lengthOfYear=" + lengthOfYear + ", lengthOfYearUnit=" + lengthOfYearUnit + ", imageName="
				+ imageName + ", hasMoons=" + hasMoons + ", isTerrestrial=" + isTerrestrial + ", atmosphereDescription="
				+ atmosphereDescription + "]";
	}
	
	
}
