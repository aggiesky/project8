package com.vadenent;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;



@Controller
@RequestMapping("/")
public class TableController {
	
	@Autowired
	PlanetRepository planetRepository;
	
	@Autowired
	UserLoginStatus loginStatus;
	
	@Autowired
	UserManager currentUserList;
	
	@Autowired
	UserData newUser;
	
	@Autowired
	Planet planet;
	

	
	@GetMapping("/table")
	public String tableView(Model model) {

		model.addAttribute("planets", planetRepository.findAll());
		model.addAttribute("loginStatus", loginStatus);
		
		return "table";
		
	}
	
	
	@GetMapping("/edit/{id}")
	public String editView(Model model, @PathVariable Long id) {

		System.out.printf("Made it to the editView controller.\n");

		model.addAttribute("loginStatus",loginStatus);
		
		Optional<Planet> optionalPlanet = planetRepository.findById(id);
		
		if (optionalPlanet.isPresent()) {
			model.addAttribute("planet", optionalPlanet.get());
			return "edit";
		} else
			
			throw new ResponseStatusException(HttpStatus.NOT_FOUND);
		
	}
	
	@PostMapping("/edit/{id}")
	public String editUpdate(Model model, @PathVariable Long id, Planet planet, RedirectAttributes redirect) {
		
		System.out.printf("Made it to the editUpdate controller.\n");
		
		if (loginStatus.isLoggedIn()) {					// block update if not logged in
			
			model.addAttribute("loginStatus", loginStatus);
			planetRepository.save(planet);
			redirect.addFlashAttribute("message", "You successfully updated planet " + planet.getPlanetName());
	
			return "redirect:/table";
			
		}
		
		return "edit";
	}


	@GetMapping("/new")
	public String newView(Model model, @ModelAttribute Planet newPlanet) {
		
		System.out.printf("Made it to the newView controller.\n");

		model.addAttribute("loginStatus", loginStatus);
		model.addAttribute("planet", newPlanet);
				
		return "new";
		
	}


	
	@PostMapping("/new")
	public String newUpdate(Model model, @Valid Planet newPlanet, Errors errors, RedirectAttributes redirect) {
		
		System.out.printf("Made it to the newUpdate controller.\n");
		model.addAttribute("loginStatus", loginStatus);

		if (loginStatus.isLoggedIn()) {			// block update if not logged in
			
			if (errors.hasErrors()) {
				return "new";
			} else {
				model.addAttribute("planet", newPlanet);			
				planetRepository.save(newPlanet);	
				redirect.addFlashAttribute("message", "You successfully added the new planet " + newPlanet.getPlanetName());
				return "redirect:/table";
			}
			
		}
		
		return "new";
	}

	
	@PostMapping("/delete/{id}")
	public String delete(Model model, @PathVariable Long id, RedirectAttributes redirect) {
	
		model.addAttribute("loginStatus", loginStatus);
		
		System.out.printf("Made it to the delete controller.\n");
		
		if (loginStatus.isLoggedIn()) {				// block deletion if not logged in
			
			Optional<Planet> optionalPlanet = planetRepository.findById(id);
			
			if (optionalPlanet.isPresent()) 
				planet = optionalPlanet.get();
			else	
				throw new ResponseStatusException(HttpStatus.NOT_FOUND);
			
			planetRepository.deleteById(id);
	
			redirect.addFlashAttribute("message", "You successfully deleted planet " + planet.getPlanetName());
		}
		
		return "redirect:/table";
	}
		

	
}		// end of class TableController
