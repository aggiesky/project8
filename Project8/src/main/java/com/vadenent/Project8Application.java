package com.vadenent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.context.annotation.ApplicationScope;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.context.annotation.SessionScope;

@SpringBootApplication
public class Project8Application {

	public static void main(String[] args) {
		SpringApplication.run(Project8Application.class, args);
	}

	
	// Beans in Session Scope
	@Bean("loginStatus")
	@SessionScope
	public UserLoginStatus makeUserSession() {
		UserLoginStatus userStatus = new UserLoginStatus();
		userStatus.logout();
		return userStatus;
	}
	
	
	
	// Beans in Request Scope  --------------------------------------------------------
	@Bean("newUser")
	@RequestScope
	public UserData makeNewUser() {
		UserData newUser = new UserData();
		return newUser;
	}
	
	@Bean("planet")
	@RequestScope
	public Planet createPlanet() {
		Planet newPlanet = new Planet();
		return newPlanet;
	}
	
	
	
	// Beans in Application Scope  ----------------------------------------------------
	@Bean("currentUserList")
	@ApplicationScope
	public UserManager createListOfUsers() {
		UserManager listOfUsers = new UserManager();
		return listOfUsers;
	}
	

	
	// Beans in Session Scope  -------------------------------------------------------
	@Bean("formDataLogin")
	@SessionScope
	public FormDataLogin createFormDataLogin() {
		FormDataLogin form = new FormDataLogin();
		return form;
	}
	
	
}
