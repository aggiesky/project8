package com.vadenent;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class PasswordHash {

	// entry point
	public static String hash(String password) {
		return Base64.getEncoder().encodeToString(	owaspHash(password.toCharArray(), 
													"badsalt".getBytes(), 
													10000, 256));
	}

	// https://www.owasp.org/index.php/Hashing_Java
	private static byte[] owaspHash(final char[] password, 
									final byte[] salt, 
									final int iterations, 
									final int keyLength) {

		try {
			SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
			PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, keyLength);
			SecretKey key = skf.generateSecret(spec);
			byte[] res = key.getEncoded();
			return res;

		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new RuntimeException(e);
		}
	}

}
