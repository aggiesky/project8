package com.vadenent;


public class FormDataLogin {

	private String userName;
	private String password;

	// userName
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}

	// password
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	
	@Override
	public String toString() {
		return "UserData [userName=" + userName + ", password=" + password + "]";
	}
	
	
}
