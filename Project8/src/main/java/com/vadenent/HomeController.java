package com.vadenent;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

@Controller
@RequestMapping("/")
public class HomeController {
	
	@Autowired
	PlanetRepository planetRepository;
	
	@Autowired
	UserLoginStatus loginStatus;
	
	@Autowired
	UserManager currentUserList;
	
	@Autowired
	UserData newUser;
	
	
	
	// main page
	@GetMapping("/")
	public String homeView( Model model) {

		model.addAttribute("planets", planetRepository.findAll());
		model.addAttribute("loginStatus", loginStatus);
		
		return "master";
		
	}
	
	
	
	// details page
	@GetMapping("/details/{id}")
	public String detailsView(Model model, @PathVariable Long id) {
		
		Optional<Planet> planet = planetRepository.findById(id);
		
		if (planet.isPresent()) {
	
			model.addAttribute("planet",  planet.get());
			model.addAttribute("loginStatus", loginStatus);
			return "details";
			
		} else 

			throw new ResponseStatusException(HttpStatus.NOT_FOUND);	
		

	}	// end method detailsView

	
}		// end of class HomeController
