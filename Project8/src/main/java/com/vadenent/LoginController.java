package com.vadenent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;




@Controller
@RequestMapping("/")
public class LoginController {

	@Autowired
	UserLoginStatus loginStatus;
	
	@Autowired
	UserData newUser;
	
	@Autowired
	UserManager currentUserList;
	
	@Autowired
	FormDataLogin formDataLogin;
	
	
	
	@GetMapping("/login")
	public String loginView(Model model) {
		
		model.addAttribute("loginStatus", loginStatus);
		model.addAttribute("formDataLogin", formDataLogin);
		return "login";
	}
	
	
	
	
	@PostMapping("/login")
	public String loginUpdate(Model model, FormDataLogin loginForm, RedirectAttributes redirect) {
		
		model.addAttribute("loginStatus", loginStatus);
		model.addAttribute("formDataLogin", formDataLogin);
		
		newUser.setUserName(loginForm.getUserName());
		newUser.setPassword(loginForm.getPassword());
				
		if (currentUserList.isRegistered(newUser)) {
			
			if (currentUserList.isPasswordValid(newUser)) {
	
				//set current User as logged in; place in the model (session scope)
				loginStatus.loginAs(newUser.getUserName());
				
				//set flash message - login successful
				redirect.addFlashAttribute("message", "You logged in successfully as '" + loginForm.getUserName() + "'");
				return "redirect:/";

			} else {	// report invalid password
				model.addAttribute("message", "Login failed.  Try Again.");
				return "login";
			}
				
		} else {	// report the user is not registered - please register first
			model.addAttribute("message", "The 'Username' is not registered.  Please select 'New User Registration'");
			return "login";
		}
		

	}
	
	
		
	
	// === logout function ==========================================
	@GetMapping("/logout")
	public String logoutView(Model model, RedirectAttributes redirect) {
		
		model.addAttribute("loginStatus", loginStatus);
		
		loginStatus.logout();

		redirect.addFlashAttribute("message", "You have logged out.  Good-bye.");

		return "redirect:/";
	}


	
}
