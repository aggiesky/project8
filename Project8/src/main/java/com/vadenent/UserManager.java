package com.vadenent;

import java.util.HashMap;

public class UserManager {
	
	private HashMap<String, String> currentListOfUsers;

	
	// constructor
	public UserManager() {
		currentListOfUsers = new HashMap<>();
	}
	
	
	// check the entered password against the saved password for a given username
	public boolean isPasswordValid(UserData theUser) {
		
		// add hashing on the input password
		// get the saved password which is hashed
		// compare the two - true iff they match
		
		String hashedPassword = PasswordHash.hash(theUser.getPassword());
				
		if ( currentListOfUsers.get(theUser.getUserName()).equals(hashedPassword) )
		   return true;
		else
		   return false;
		   
	}
	
	
	// Add a new element in the ArrayList called currentListOfUsers
	public String addUser(UserData newUser) {	
		
		// generate a hashed version of the password
		// save the hashed version of the password with the userName in currentListOfUsers
		
		String hashedPassword = PasswordHash.hash(newUser.getPassword());

		if (! currentListOfUsers.containsKey(newUser.getUserName()) ) {
			currentListOfUsers.put(newUser.getUserName(), hashedPassword);		
			return "Successful";
		} else {
			return "Failed";
		}
	}

	public boolean isRegistered(UserData newUser) {
		// true if the newUser.userName is already in the currentListOfUsers
		return currentListOfUsers.containsKey(newUser.getUserName());
	}
	
	// this is here to aid debug, although it is available for normal functions
	public void listUsers() {
		for (String userId : currentListOfUsers.keySet()) {
			System.out.printf(">>> UserID:  %s\n", userId);
		}
		System.out.println();
		
	}
	
}
