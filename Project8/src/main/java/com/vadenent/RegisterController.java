package com.vadenent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
@RequestMapping("/")
public class RegisterController {
	
	@Autowired
	UserLoginStatus loginStatus;
	
	@Autowired
	UserData newUser;
	
	@Autowired
	UserManager currentUserList;
	
	
	
	// === Register a new user page ==========================================
	@GetMapping("/register")
	public String registerView(Model model, FormDataRegister registerForm) {
		
		model.addAttribute("loginStatus", loginStatus);
		model.addAttribute("formDataRegister", registerForm);
		
		return "register";
	}

	
	@PostMapping("/registerUpdate")
	public String registerUpdate(Model model, FormDataRegister registerForm, RedirectAttributes redirect) {
		
		model.addAttribute("loginStatus", loginStatus);

		newUser.setUserName(registerForm.getUserName());
		newUser.setPassword(registerForm.getPassword1());
		
		if (currentUserList.isRegistered(newUser)) { 	// User is registered already
			
			model.addAttribute("message", "This UserID " + newUser.getUserName() + " is already registered.");
			return "register";
			
		} else {
			
			if (registerForm.checkPassword()) {		// Not registered and passwords match
				currentUserList.addUser(newUser);
			//	currentUserList.listUsers();
				redirect.addFlashAttribute("message", "You registered successfully as '" + registerForm.getUserName() + "'");
				return "redirect:/";
				
			} else {								// Not registered and passwords don't match
				
				model.addAttribute("message", "Your passwords did not match.  Try again.");
				return "register";
				
			} 

		}  
		
	}  // end of method registerUpdate
	
	
}		// end of class RegisterController
