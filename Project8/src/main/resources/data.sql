insert into PLANET 
(ID, PLANET_NAME, DISTANCE_FROM_SUN, LENGTH_OF_YEAR, LENGTH_OF_YEAR_UNIT, IMAGE_NAME, HAS_MOONS, NUMBER_OF_MOONS, IS_TERRESTRIAL, ATMOSPHERE_DESCRIPTION)
values
(1, 'Mercury', 49, 88, 'days', 'mercury.jpg', false, 'no moons', true, 'There is no atmosphere to speak of.'),
(2, 'Venus', 107, 225, 'days', 'venus.jpg', false, 'no moons', true, 'The atmosphere is crazy hot and composed mainly of CO2. The surface temperature is 470+C.'),
(3, 'Earth', 151, 365, 'days', 'earth.png', true, '1 moon', true, 'The atmosphere is composed mainly of nitrogen and oxygen, with clouds formed of condensed water droplets.'),
(4, 'Mars', 249, 687, 'days', 'mars.png', true, '2 moons', true, 'Mars has a thin atmosphere made up mostly of CO2, argon, nitrogen, and a small amount of oxygen and water vapor.'),
(5, 'Jupiter', 790, 11.9, 'years', 'jupiter.jpg', true, '79 moons', false, 'The atmosphere on Jupiter is full of clouds that form pronounced horizontal bands due to the rapid rotation of the planet.');

