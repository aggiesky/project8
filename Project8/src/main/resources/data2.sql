insert into NATIONAL_PARKS 
(ID, PARK_NAME, PARK_LOCATION, STATE, DESCRIPTION, GEOGRAPHY, IMAGE_FILE, FOUNDING_YEAR)
values
(1, 'Rocky Mountain National Park', 'front range of the Rockies in northern Colorado', 'Colorado', 'cool', 'mountains', 'a picture', 1916 ),
(2, 'Grand Canyon National Park', 'northern Arizona', 'Arizona', 'awesome', 'desert', 'a picture', 1919 ),
(3, 'Yosemite National Park', 'central California', 'California', 'peaceful', 'mountains', 'a picture', 1864 ),
(4, 'Yellowstone National Park', 'northwestern Wyoming, Montana and Idaho', 'Wyoming', 'crazy', 'mountains', 'a picture', 1872),
(5, 'Hawaii Volcanoes National Park', 'the big island of Hawaii', 'Hawaii', 'spectacular', 'volcanoes', 'a picture', 1916);
